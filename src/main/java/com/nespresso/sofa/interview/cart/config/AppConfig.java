package com.nespresso.sofa.interview.cart.config;

import com.nespresso.sofa.interview.cart.services.CartService;
import com.nespresso.sofa.interview.cart.services.PromotionEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.nespresso.sofa.interview.cart.services.CartStorage;

@Configuration
public class AppConfig {

    @Autowired
    public static ApplicationContext applicationContext;

    @Bean
    public CartStorage cartStorage() {
        return new CartStorage();
    }
    @Bean
    public CartService cartService() {
        return new CartService();
    }

    @Bean
    public PromotionEngine promotionEngine() {
        return new PromotionEngine();
    }

}
