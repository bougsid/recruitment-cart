package com.nespresso.sofa.interview.cart.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static java.util.UUID.randomUUID;

public final class Cart implements Serializable {

    private final UUID id;
    private Map<String, Integer> products = new LinkedHashMap<>();

    public Cart() {
        this(randomUUID());
    }

    public Cart(UUID id) {
        this.id = id;
    }

    public Cart(Map<String, Integer> products) {
        this.id = randomUUID();
        this.products = products;
    }

    public UUID getId() {
        return id;
    }

    public Map<String, Integer> getProducts() {
        return products;
    }

    @Override
    public String toString() {
        return "Cart {" +
                "id: " + id +
                ", products: " + products +
                '}';
    }

    public void removeProduct(String productCode) {
        Map<String, Integer> products = new LinkedHashMap<>(this.products);
        products.entrySet().removeIf(product -> product.getKey().equals(productCode));
        this.products = products;
    }

    public boolean add(String productCode, int quantity) {
        if (quantity <= 0) return false;
        this.products.put(productCode, this.products.getOrDefault(productCode, 0) + quantity);
        return true;
    }

    public boolean setQuantity(String productCode, int quantity) {
        Optional<Map.Entry<String, Integer>> optionalProduct = getProductByCode(productCode);
        if (optionalProduct.isPresent()) {
            Map.Entry<String, Integer> product = optionalProduct.get();
            if (quantity <= 0) {
                this.removeProduct(productCode);
                return true;
            }
            if (product.getValue() == quantity) return false;
        }
        Map<String, Integer> products = new LinkedHashMap<>(this.products);
        products.put(productCode, quantity);
        this.products  =products;
        return true;
    }

    private Optional<Map.Entry<String, Integer>> getProductByCode(String productCode) {
        return this.products.entrySet().stream()
                .filter(p -> p.getKey().equals(productCode))
                .findFirst();
    }

    public int quantity() {
        int quantity = 0;
        for (Map.Entry<String, Integer> product : this.products.entrySet()) {
            quantity += product.getValue();
        }
        return quantity;
    }
}
