package com.nespresso.sofa.interview.cart.services;

import com.nespresso.sofa.interview.cart.model.Cart;

/**
 * If one or more product with code "1000" is purchase, ONE product with code 9000 is offer
 * For each 10 products purchased a gift with code 7000 is offer.
 */
public class PromotionEngine {
    public static final String PRODUCT_WITH_PROMOTION = "1000";
    public static final String PROMOTION = "9000";
    public static final String GIFT = "7000";

    private static final int ELIGIBLE_FOR_GIFT_QUANTITY = 10;


    public Cart apply(Cart cart) {
        this.checkCartForPromotion(cart);
        this.checkCartForGift(cart);
        this.applyPromotionIfElligible(cart);
        return cart;
    }

    private void checkCartForPromotion(Cart cart) {
        if (notEligibleForPromotion(cart)) {
            cart.removeProduct(PROMOTION);
        }
    }

    private void checkCartForGift(Cart cart) {
        if (notEligibleForGift(cart)) {
            cart.removeProduct(GIFT);
        }
        addGiftIfEligible(cart);
    }

    private void applyPromotionIfElligible(Cart cart) {
        if (cartHasPromotionProduct(cart)) {
            cart.setQuantity(PROMOTION, 1);
        }
    }


    private void addGiftIfEligible(Cart cart) {
        if (cart.quantity() >= ELIGIBLE_FOR_GIFT_QUANTITY)
            cart.setQuantity(GIFT, cart.quantity() / ELIGIBLE_FOR_GIFT_QUANTITY);
    }

    private boolean cartHasPromotionProduct(Cart cart) {
        return cart.getProducts().containsKey(PRODUCT_WITH_PROMOTION);
    }

    private boolean notEligibleForPromotion(Cart cart) {
        return cart.getProducts().containsKey(PROMOTION) &&
                !cart.getProducts().containsKey(PRODUCT_WITH_PROMOTION);
    }


    private boolean notEligibleForGift(Cart cart) {
        return cart.getProducts().containsKey(GIFT) &&
                cart.quantity()<ELIGIBLE_FOR_GIFT_QUANTITY;
    }
}
